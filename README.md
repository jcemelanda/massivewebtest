# Massive Web Test

## Shape Calculator

### Assignment
Create a web application that calculates the area of geometric figures. It should consist of a backend implemented in Python and a frontend using tech of your choice. Wireframe design with usage walkthrough is attached (shapecalculator.pdf).

### Requirements

* Your code should be simple to read and understand (“self-documented”)
* All parameters should have sane default values
* You should be able to explain your design choices
* Calculator logic should be separated from presentation
* It should be easy to add new shape types in the future
* Implemented shapes:
    * Circle (requires diameter)
    * Ellipse (requires height and width)
    * Square (requires width)
    * Rectangle (requires height and width)
* Instructions for setting up and running the application should be included

### Shapecalculator.pdf
#### Image 1
Display header with text (images/header_bg.gif). To the left is a short informative text of how to use the application, and some dummy text. To the right there is a radio button list of available shapes. User should select one and click next.
#### Image 2
Step 2 includes a form with labels and input fields required for calculating the area of the selected shape. Be aware that different shapes require different input fields. Clicking “Go to step 3” should trigger the calculation.
#### Image 3
The result is a dynamic text based on what the user have selected in the previous steps and the calculated area. The submit button takes the user to step 1 again.

## How to run the application

### Python Version

This project requires Python 3.5 or newer

### Requirements

The requirements are listed in the requirements.txt file.
To install them just run 

    pip install -r requirements.txt
    
from inside the project folder. Make sure you have a working python installation first. I recomend using some kind of virtual environment like `virtualenv`.

### Setting the project

After the requirements are installed, just run

    python manage.py migrate
    
This will create a SQLite3 database with the initial shapes loaded

Then, create a superuser with

    python manage.py createsuperuser
    
Finally, execute the application with

    python manage.py runserver

### Adding new shapes

To add new shapes go to the admin page of the website (localhost:8000/admin for the default runserver instance) and add shapes them in the admin interface.

There you'll need to input a shape name, and an expression where you'll be able to use parameters you set below in the parameters section.

The expression may be composed of variables you insert in the parameters, math operators and the PI number, that should be called "pi".

You can always check the default shapes to have examples.
