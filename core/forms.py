from django.forms import Form, CharField, FloatField


class HomeForm(Form):
    shape = CharField(max_length=50)


class ShapeForm(Form):
    def __init__(self, parameters, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for parameter in parameters:
            field_name = parameter.name.lower()
            self.fields[field_name] = FloatField()
