from django.db import models

class Shape(models.Model):
    name = models.CharField(max_length=50, unique=True)
    area_expression = models.TextField(help_text="You should use the parameter variables here. PI should be written pi and powers should be written as '**'")

    def __str__(self):
        return self.name

class Parameter(models.Model):
    name = models.CharField(max_length=30)
    shape = models.ForeignKey(Shape, on_delete=models.CASCADE)
    variable = models.CharField(max_length=10)

    class Meta:
        unique_together = ('variable', 'shape')

    def __str__(self):
        return self.name

