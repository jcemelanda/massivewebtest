from django.shortcuts import render, redirect
from django.views import View

from core.forms import HomeForm, ShapeForm
from core.models import Shape

import math
import numexpr

class HomeView(View):
    template_name = 'core/home.html'
    def get(self, request):
        shapes = Shape.objects.all()
        return render(request, self.template_name, {'shapes': shapes})

    def post(self, request):
        form = HomeForm(request.POST)
        if form.is_valid():
            return redirect('shape', shape=form.cleaned_data['shape'].lower())
        return render(request, self.template_name, {'shapes': Shape.objects.all()})


class ShapeView(View):
    template_name = 'core/shape.html'

    def get(self, request, shape):
        shape = Shape.objects.get(name=shape.capitalize())
        parameters = shape.parameter_set.all()
        return render(request, self.template_name, {'parameters': parameters, 'shape': shape.name.lower()})

    def post(self, request, shape):
        shape = Shape.objects.get(name=shape.capitalize())
        form = ShapeForm(shape.parameter_set.all(), request.POST)
        parameters = shape.parameter_set.all()
        if form.is_valid():
            expression = shape.area_expression
            if 'pi' in expression:
                pi = math.pi
            for parameter in parameters:
                locals()[parameter.variable] = form.cleaned_data[parameter.name.lower()]
                parameter.value = form.cleaned_data[parameter.name.lower()]
            value = numexpr.evaluate(expression).item()
        return render(request, self.template_name, {'shape': shape.name.lower(), 'value': value, 'parameters': parameters})
