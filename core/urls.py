from django.urls import path

from core.views import HomeView, ShapeView

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('<slug:shape>/', ShapeView.as_view(), name='shape')
]