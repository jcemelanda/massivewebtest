from django.contrib import admin

from core.models import Parameter
from core.models import Shape


class ParameterInline(admin.StackedInline):
    model = Parameter
    extra = 0

class ShapeAdmin(admin.ModelAdmin):
    inlines = (ParameterInline,)
    list_display = ('name', 'area_expression')

admin.site.register(Shape, ShapeAdmin)
